//
// Created by markp on 18.10.2019.
//

#ifndef WINAPI_APPLICATION_H
#define WINAPI_APPLICATION_H

#include "models.h"

typedef enum {TYPESET, DEFAULT} mode;


typedef struct {
    view_model view;
    store_model store;
    mode mode;
} application;

#endif //WINAPI_APPLICATION_H
