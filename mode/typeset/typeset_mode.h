//
// Created by markp on 18.10.2019.
//

#ifndef WINAPI_TYPESET_MODE_H
#define WINAPI_TYPESET_MODE_H

#include "../../models.h"
/**
 * @var line         - quantity of line
 * @var sym          - quantity of sym
 */
typedef struct {
    long line;
    int sym;
} border;

/**
 *
 * @var isInit        - flag to check state of auxiliaryTS
 * @var top           - number of first visible line and quantity of visible symbols of it
 * @var bottom        - number of last visible line and quantity of visible symbols of it
 * @var updateBottom  - interface to throw border of invalid rectangle into function drawRect
 * @var updateTop     - interface to throw border of invalid rectangle into function drawRect
 */
typedef struct {
    int isInit;
    border top;
    border bottom;
    border updateBottom;
    border updateTop;
} auxiliaryTS;

LRESULT tsWindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam, store_model * store, view_model * view);
void startTypesetFromView(HWND hwnd, store_model * store, view_model * view);
long getCurrLineAndFinish(view_model * view);

#endif //WINAPI_TYPESET_MODE_H

