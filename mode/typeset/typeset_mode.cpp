//
// Created by markp on 18.10.2019.
//

#include <cstdio>
#include <windows.h>
#include "typeset_mode.h"

#include "../../config.h"
#include "../../models.h"


#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))

#define AUXILIARY(view) ((auxiliaryTS *)((view)->auxiliary))

#define DOWN 1
#define UP -1
#define LEFT -1
#define RIGHT 1

/**
 *
 * @brief drop bottom border by one logic line
 */
static void downBottomBorder(view_model * view, store_model * store) {
    border * bottom = &(AUXILIARY(view)->bottom);

    int bottomRemnant = strlen(store->lines[bottom->line]) - bottom->sym;

    if (bottomRemnant >= view->winSizeSyms.x) {
        bottom->sym += view->winSizeSyms.x;
    } else if(bottom->sym == strlen(store->lines[bottom->line])) {
        bottom->line++;
        bottom->sym = min(view->winSizeSyms.x, strlen(store->lines[bottom->line]));
    } else {
        bottom->sym = strlen(store->lines[bottom->line]);
    }
}
/**
 *
 * @brief drop top border by one logic line
 */
static void downTopBorder(view_model * view, store_model * store) {
    border * top = &(AUXILIARY(view)->top);
    int topRemnant = top->sym;
    if (topRemnant > view->winSizeSyms.x) {
        top->sym -= view->winSizeSyms.x;
    } else {
        top->line++;
        top->sym = strlen(store->lines[top->line]);
    }
}
/**
 *
 * @brief drop borders by one logic line
 */
static void downBorder(view_model * view, store_model * store) {
    downTopBorder(view, store);
    downBottomBorder(view, store);
}

/**
 *
 * @brief pick up top border on one logic line
 */
static void upTopBorder(view_model * view, store_model * store) {
    border * top = &(AUXILIARY(view)->top);
    int topRemnant = strlen(store->lines[top->line]) - top->sym;
    if (topRemnant >= view->winSizeSyms.x) {
        top->sym += view->winSizeSyms.x;
    } else {
        top->line--;
        int len = strlen(store->lines[top->line]);
        if (len % view->winSizeSyms.x != 0) {
            top->sym = len % view->winSizeSyms.x;
        } else {
            top->sym = view->winSizeSyms.x;
        }
    }
}
/**
 *
 * @brief pick up bottom border on one logic line
 */
static void upBottomBorder(view_model * view, store_model * store) {
    border * bottom = &(AUXILIARY(view)->bottom);
    int bottomRemnant = bottom->sym;

    if (bottomRemnant > view->winSizeSyms.x) {

        if (bottomRemnant % view->winSizeSyms.x != 0) {
            bottom->sym -= bottomRemnant % view->winSizeSyms.x;
        } else {
            bottom->sym -= view->winSizeSyms.x;
        }

    } else {
        bottom->line--;
        bottom->sym = strlen(store->lines[bottom->line]);
    }
}
/**
 *
 * @brief pick up borders on one logic line
 */
static void upBorder(view_model * view, store_model * store) {
    upTopBorder(view, store);
    upBottomBorder(view, store);
}


/**
 *
 * @brief @brief function handles a vscroll action, updates scrolls and borders, scrolls windows and calls InvalidRect if it was appear
 */
static void wm_vscroll_handler(HWND hwnd, LPARAM lParam, WPARAM wParam, store_model * store, view_model * view) {
    long realScrollYInc = 0;
    switch(LOWORD(wParam)) {
        case SB_LINEUP:
            if (view->realScrollPos.y > 0)
                realScrollYInc = UP;
            break;
        case SB_LINEDOWN:
            if (view->realScrollPos.y < view->realScrollRange.y)
                realScrollYInc = DOWN;
            break;
        case SB_PAGEUP :
            break;
        case SB_PAGEDOWN :
            break;
        case SB_THUMBPOSITION :
            realScrollYInc = HIWORD(wParam) - view->realScrollPos.y;
            break;
        default : break;
    }

    if (realScrollYInc) {
        int logicScrollY = view->logicScrollPos.y;
        view->realScrollPos.y += realScrollYInc;
        view->logicScrollPos.y = logicYFromReal(view);
        int logicScrollYInc = view->logicScrollPos.y - logicScrollY;

        if (logicScrollYInc > 0) {
            if (logicScrollYInc < view->winSizeSyms.y) {
                AUXILIARY(view)->updateTop = AUXILIARY(view)->bottom;
                for (int i = 0; i < logicScrollYInc; ++i) {
                    downBorder(view, store);
                }
                AUXILIARY(view)->updateBottom = AUXILIARY(view)->bottom;
            } else {
                for (int i = 0; i < logicScrollYInc; ++i) {
                    downBorder(view, store);
                }
                AUXILIARY(view)->updateTop = AUXILIARY(view)->top;
                AUXILIARY(view)->updateTop.sym = strlen(store->lines[AUXILIARY(view)->top.line]) - AUXILIARY(view)->top.sym;
                AUXILIARY(view)->updateBottom = AUXILIARY(view)->bottom;
            }
        } else {
            if (logicScrollYInc < view->winSizeSyms.y) {
                AUXILIARY(view)->updateBottom.line = AUXILIARY(view)->top.line;
                AUXILIARY(view)->updateBottom.sym =
                        strlen(store->lines[AUXILIARY(view)->top.line]) - AUXILIARY(view)->top.sym;
                for (int i = 0; i < -logicScrollYInc; ++i) {
                    upBorder(view, store);
                }
                AUXILIARY(view)->updateTop.line = AUXILIARY(view)->top.line;
                AUXILIARY(view)->updateTop.sym = strlen(store->lines[AUXILIARY(view)->top.line]) - AUXILIARY(view)->top.sym;
            } else {
                for (int i = 0; i < -logicScrollYInc; ++i) {
                    upBorder(view, store);
                }
                AUXILIARY(view)->updateTop = AUXILIARY(view)->top;
                AUXILIARY(view)->updateTop.sym = strlen(store->lines[AUXILIARY(view)->top.line]) - AUXILIARY(view)->top.sym;
                AUXILIARY(view)->updateBottom = AUXILIARY(view)->bottom;
            }
        }
        PRINT_BORDER_SCROLL(view);
        SetScrollPos(hwnd, SB_VERT, view->realScrollPos.y, TRUE);
        ScrollWindow(hwnd, 0, -logicScrollYInc * view->symSize.y, &(view->clientRect), NULL);
        SendMessage(hwnd, WM_INVALIDRECT_BOTTOM, NULL, NULL);
    }
}


/**
 *
 * @brief function handles a keydown action, updates scrolls and borders, scrolls windows and calls InvalidRect if it was appear
 */
static void wm_keydown_handler(HWND hwnd, LPARAM lParam, WPARAM wParam, store_model * store, view_model * view) {
    int scrollYInc = 0;
    switch (wParam) {
        case VK_DOWN:
            if (view->logicScrollPos.y < view->logicScrollRange.y) {
                scrollYInc = DOWN;

                AUXILIARY(view)->updateTop = AUXILIARY(view)->bottom;
                downBorder(view, store);
                AUXILIARY(view)->updateBottom = AUXILIARY(view)->bottom;
            }
            break;
        case VK_UP:
            if (view->logicScrollPos.y > 0) {
                scrollYInc = UP;

                AUXILIARY(view)->updateBottom.line = AUXILIARY(view)->top.line;
                AUXILIARY(view)->updateBottom.sym = strlen(store->lines[AUXILIARY(view)->top.line]) - AUXILIARY(view)->top.sym;
                upBorder(view, store);
                AUXILIARY(view)->updateTop.line = AUXILIARY(view)->top.line;
                AUXILIARY(view)->updateTop.sym = strlen(store->lines[AUXILIARY(view)->top.line]) - AUXILIARY(view)->top.sym;

                SendMessage(hwnd, WM_INVALIDRECT_BOTTOM, NULL, NULL);
            }
            break;
    }

    if (scrollYInc) {
        view->logicScrollPos.y += scrollYInc;
        view->realScrollPos.y = realYFromLogic(view);
        SetScrollPos(hwnd, SB_VERT, view->realScrollPos.y, TRUE);

        ScrollWindow(hwnd, 0, -scrollYInc * view->symSize.y, &(view->clientRect), NULL);
        PRINT_BORDER_SCROLL(view);
    }
}


/**
 *
 * @brief count a border using a scroll position
 */
static void initBorder(view_model * view, store_model * store) {
    AUXILIARY(view)->isInit = 0;
    AUXILIARY(view)->top.sym = strlen(store->lines[AUXILIARY(view)->top.line]);
    AUXILIARY(view)->bottom.line = AUXILIARY(view)->top.line;
    AUXILIARY(view)->bottom.sym = 0;

    for (int i = 0; i < view->winSizeSyms.y; ++i) {
        if  (AUXILIARY(view)->bottom.line < store->capacity.y-1) {
            downBottomBorder(view, store);
        }
        else {
            upTopBorder(view, store);
        }
    }

    AUXILIARY(view)->updateTop.line = AUXILIARY(view)->top.line;
    AUXILIARY(view)->updateTop.sym = strlen(store->lines[AUXILIARY(view)->top.line]) - AUXILIARY(view)->top.sym;
    AUXILIARY(view)->updateBottom = AUXILIARY(view)->bottom;
}
/**
 *
 * @brief  recalculate number of lines for typeset mode and find a new position of first visible line
 */
static void initScroll(view_model * view, store_model * store) {
    long cursor = 0;
    for (long i = 0; i < store->capacity.y; ++i) {


        if (i == AUXILIARY(view)->top.line) {
            view->logicScrollPos.y = cursor;
        }
        int len = strlen(store->lines[i]);
        if (len == 0)
            cursor++;
        cursor += len / view->winSizeSyms.x + (len % view->winSizeSyms.x ? 1 : 0);
    }
    view->logicScrollRange.y = cursor - view->winSizeSyms.y;
    view->logicScrollPos.y = min(view->logicScrollPos.y, view->logicScrollRange.y);
    view->realScrollPos.y = realYFromLogic(view);
}
/**
 *
 * @brief update scrolls, init border using scroll position, call InvalidRect for clientRect
 */
static void wm_size_handler(HWND hwnd, LPARAM lParam, WPARAM wParam, store_model * store, view_model * view) {
    view->realScrollRange = {0, 100};

    initScroll(view, store);
    SetScrollPos(hwnd, VK_SCROLL, view->realScrollPos.y, TRUE);
    initBorder(view, store);

    PRINT_SIZE(view);
    PRINTE_COUNT_OF_LINES(store);
    PRINT_BORDER_SCROLL(view);

    InvalidateRect(hwnd, &(view->clientRect), TRUE);
    InvalidateRect(hwnd, &(view->invalidRectRight), TRUE);
}


/**
 *
 * @return default value for auxiliary type
 */
auxiliaryTS * initTSAuxiliary() {
    auxiliaryTS * res = (auxiliaryTS *)malloc(sizeof(auxiliaryTS));
    res->top = {0, 0};
    res->bottom = {0 , 0};
    res->isInit = 0;
    return res;
}
/**
 *
 * @brief initialize a auxiliary date for typeset mode
 */
static void wm_create_handler(HWND hwnd, LPARAM lParam, WPARAM wParam, store_model * store, view_model * view) {
    view->auxiliary = initTSAuxiliary();
}


/**
 *
 * @param rect  - invalid rect
 * @brief It function fill the rectangle using prepared start and finish logic coords
 */
static void drawRect(HDC hdc, RECT rect, view_model * view, store_model * store) {
    border * top = &(AUXILIARY(view)->updateTop);
    border * bottom = &(AUXILIARY(view)->updateBottom);

    int lineCursor = top->line;
    int symCursor = top->sym;

    if (top->sym == strlen(store->lines[top->line])) {
        lineCursor++;
        symCursor = 0;
    }

    int x = rect.left;
    int y = rect.top;

    while (lineCursor < bottom->line || (lineCursor == bottom->line && symCursor < bottom->sym)) {
        int len = strlen(store->lines[lineCursor] + symCursor);

        if (len > view->winSizeSyms.x) {
            len = view->winSizeSyms.x;
            TextOut(hdc, x, y, store->lines[lineCursor] + symCursor, len);
            symCursor += len;
        } else if (len == view->winSizeSyms.x) {
            len = view->winSizeSyms.x;
            TextOut(hdc, x, y, store->lines[lineCursor] + symCursor, len);
            lineCursor++;
            symCursor = 0;
        } else {
            TextOut(hdc, x, y, store->lines[lineCursor] + symCursor, len);
            lineCursor++;
            symCursor = 0;
        }



        y += view->symSize.y;
    }
}


/**
 *
 * @brief finish typeset mode: free a auxiliary memory
 * @return return current first visible line
 */
long getCurrLineAndFinish(view_model * view) {
    long logicScrollPosY = AUXILIARY(view)->top.line;
    free(view->auxiliary);
    return logicScrollPosY;
}


/**
 *
 * @brief It function was calls after general func in main.cpp and handles message in typeset mode
 */
LRESULT tsWindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam, store_model * store, view_model * view) {
    PAINTSTRUCT ps;
    HDC hdc;

    switch (message)                  /* handle the messages */
    {
        case WM_CREATE:
            PRINT_MESSEGE(WM_CREATE)
            wm_create_handler(hwnd, lParam, wParam, store, view);
            break;
        case WM_DESTROY:
            PRINT_MESSEGE(WM_DESTROY)
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;
        case WM_PAINT:
            PRINT_MESSEGE(WM_PAINT)


            hdc = BeginPaint(hwnd, &ps);


            drawRect(hdc, ps.rcPaint, view, store);
            if (ps.rcPaint.bottom >= view->invalidRectBelow.top)
                FillRect(hdc, &(view->invalidRectBelow), (HBRUSH) COLOR_BACKGROUND);
            EndPaint(hwnd, &ps);
            break;
        case WM_SIZE:
            PRINT_MESSEGE(WM_SIZE)
            wm_size_handler(hwnd, lParam, wParam, store, view);
            break;
        case WM_KEYDOWN:
            PRINT_MESSEGE(WM_KEYDOWN)
            wm_keydown_handler(hwnd, lParam, wParam, store, view);
            break;
        case  WM_VSCROLL:
            PRINT_MESSEGE(WM_VSCROLL)
            wm_vscroll_handler(hwnd,  lParam, wParam, store, view);
            break;
        case WM_QUIT:
            PRINT_MESSEGE(WM_QUIT)
            getCurrLineAndFinish(view);
            break;
        default:
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
};


/**
 *
 * @brief This function updates scrolls and save first visible line with number of logical position
 */
void startTypesetFromView(HWND hwnd, store_model * store, view_model * view) {
    view->auxiliary = initTSAuxiliary();
    AUXILIARY(view)->top.line = view->logicScrollPos.y;
    AUXILIARY(view)->top.sym = strlen(store->lines[view->logicScrollPos.y]);

    initScroll(view, store);
    initBorder(view, store);
    InvalidateRect(hwnd, &(view->clientRect), TRUE);
}