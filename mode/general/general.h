//
// Created by markp on 18.10.2019.
//

#ifndef WINAPI_GENERAL_H
#define WINAPI_GENERAL_H

#include "../../models.h"

int initStoreModel(char * fileName, store_model * store);

#endif //WINAPI_GENERAL_H
