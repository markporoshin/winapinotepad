//
// Created by markp on 18.10.2019.
//

#include <cstdio>
#include "general.h"
#include "../../models.h"

/**
 *
 * @brief       - count number of lines in the text
 * @param text  - pointer to memory with text
 * @param size  - size of text in bytes
 * @return      - number of lines in the text
 */
static int countLines(char * text, int size) {
    int i;
    int capacity = 1;
    for(i = 0; i < size; i++) {
        if (text[i] == '\n') {
            text[i] = '\0';
            capacity++;
        }
    }

    if (text[size-2] == '\0') {
        capacity--;
    } else {
        text[size-1] = '\0';
    }
    return capacity;
}


/**
 *
 * @brief           - calculate pointers to lines
 * @param text      - pointer to memory with text
 * @param size      - size of text in bytes
 * @param capacity  - number of lines in the text
 * @return          - array of pointers to lines
 */
static char ** getLines(char * text, int size, int capacity) {
    int cursor = 0;
    int i = 0;
    char ** lines = (char **)malloc(sizeof(char *) * capacity);
    do {
        lines[i++] = text + cursor;
        cursor += strlen(text+cursor) + 1;
    } while(cursor < size);
    return lines;
}


/**
 *
 * @brief           - initialize store_model using file
 * @return          - return code
 */
int initStoreModel(char * fileName, store_model * store) {
    FILE * input = fopen(fileName, "rb");
    if (input == NULL)
        return 1;

    fseek(input, 0, SEEK_END);
    int size = ftell(input) + 1;
    fseek(input, 0, SEEK_SET);

    char * buf = (char *)malloc(sizeof(char) * size);
    fread(buf, sizeof(char), size - 1, input);
    fclose(input);

    store->capacity.y = countLines(buf, size);
    store->lines = getLines(buf, size, store->capacity.y);
    store->capacity.x = strlen(*(store->lines));
    for (int i = 1; i < (store->capacity.y); ++i) {
        int len_i = strlen((store->lines[i]));
        if (len_i > store->capacity.x)
            store->capacity.x = len_i;
    }
    return 0;
}