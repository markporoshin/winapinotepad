//
// Created by markp on 18.10.2019.
//

#include "../../config.h"
//#include "default_mode.h"
#ifdef DEBUG
    #include <stdio.h>
#endif
#include "../../models.h"

#include <windows.h>

#define DOWN 1
#define UP -1
#define LEFT -1
#define RIGHT 1

/**
 *
 * @param rect - invalid rect
 * @brief - output lines in invalid rect subject to logic scroll
 */
static void drawTextRect(HDC hdc, RECT rect, view_model * view, store_model * store) {
    int rows = (rect.bottom - rect.top) / view->symSize.y;
    int startLine = (rect.top) / view->symSize.y + view->logicScrollPos.y;
    int lines = min(store->capacity.y - startLine, rows);

    int startSym = (rect.left) / view->symSize.x + view->logicScrollPos.x;
    int columns = (rect.right - rect.left) / view->symSize.x;


    for (int i = 0; i < lines; ++i) {
        int x = rect.left;
        int y = rect.top + i * view->symSize.y;

        int strLen = strlen(store->lines[i + startLine]);
        int syms = max(0, min(columns, strLen - startSym));

        TextOut(hdc, x, y, store->lines[i + startLine] + startSym, syms);
        //logTextOut(x, y, store->lines[i + startLine] + startSym, syms);
    }
}

/**
 *
 * @brief function handles a keydown action, updates scrolls, scrolls windows and calls InvalidRect if it was appear
 */
static void wm_keyboard_handler(HWND hwnd, WPARAM wParam, LPARAM lParam, view_model * view, store_model * store) {

    int scrollYInc = 0;
    int scrollXInc = 0;
    switch (wParam) {
        case VK_DOWN:
            if (view->logicScrollPos.y < view->logicScrollRange.y) {
                scrollYInc = DOWN;
            }
            break;
        case VK_UP:
            if (view->logicScrollPos.y > 0) {
                scrollYInc = UP;
                SendMessage(hwnd, WM_INVALIDRECT_BOTTOM, NULL, NULL);
            }
            break;
        case VK_RIGHT:
            if (view->logicScrollPos.x < view->logicScrollRange.x) {
                scrollXInc = RIGHT;
            }
            break;
        case VK_LEFT:
            if (view->logicScrollPos.x > 0) {
                scrollXInc = LEFT;
                SendMessage(hwnd, WM_INVALIDRECT_RIGHT, NULL, NULL);
            }
            break;
    }

    if (scrollXInc || scrollYInc) {
        view->logicScrollPos.x += scrollXInc;
        view->logicScrollPos.y += scrollYInc;

        view->realScrollPos.x = realXFromLogic(view);
        view->realScrollPos.y = realYFromLogic(view);
        SetScrollPos(hwnd, SB_HORZ, view->realScrollPos.x, TRUE);
        SetScrollPos(hwnd, SB_VERT, view->realScrollPos.y, TRUE);

        ScrollWindow(hwnd, -scrollXInc * view->symSize.x, -scrollYInc * view->symSize.y, &(view->clientRect), NULL);
    }
}

/**
 *
 * @brief Function was calls after general func in the main.cpp and func updates scroll ranges, calls InvalidRect if it was appear
 */
static void wm_size_handler(HWND hwnd, WPARAM wParam, LPARAM lParam, view_model * view, store_model * store) {
    int deltaY =  view->logicScrollRange.y - max(store->capacity.y - view->winSizePixels.y / view->symSize.y, 0);
    int deltaX =  view->logicScrollRange.x - max(store->capacity.x - view->winSizePixels.x / view->symSize.x, 0);

    if (deltaY != 0) {

        RECT invalidRect = {
                0,
                max(view->clientRect.bottom - abs(deltaY) * view->symSize.y, 0),
                view->winSizePixels.x,
                view->winSizePixels.y
        };
        InvalidateRect(hwnd, &invalidRect, TRUE);
    }
    if (deltaX != 0) {
        RECT invalidRect = {
                max(view->clientRect.right - abs(deltaX) * view->symSize.x, 0),
                0,
                view->winSizePixels.x,
                view->winSizePixels.y
        };
        InvalidateRect(hwnd, &invalidRect, TRUE);
    }

    view->logicScrollRange.x = max(store->capacity.x - view->winSizePixels.x / view->symSize.x, 0);
    view->logicScrollRange.y = max(store->capacity.y - view->winSizePixels.y / view->symSize.y, 0);
    view->realScrollRange = {100, 100};
}

/**
 *
 * @brief function handles a vscroll action, updates scrolls, scrolls windows and calls InvalidRect if it was appear
 */
static void wm_vscroll_handler(HWND hwnd, WPARAM wParam, LPARAM lParam, view_model * view, store_model * store) {
    int scrollYInc = 0;
    switch(LOWORD(wParam)) {
        case SB_LINEUP:
            if (view->realScrollPos.y > 0)
                scrollYInc = UP;
            break;

        case SB_LINEDOWN:
            if (view->realScrollPos.y < view->realScrollRange.y)
                scrollYInc = DOWN;
            break;

        case SB_PAGEUP :
            break;

        case SB_PAGEDOWN :
            break;

        case SB_THUMBPOSITION :
            scrollYInc = HIWORD(wParam) - view->realScrollPos.y;
            break;
        default : break;
    }

    if (scrollYInc) {
        view->realScrollPos.y += scrollYInc;

        SetScrollPos(hwnd, SB_VERT, view->realScrollPos.y, TRUE);
        ScrollWindow(hwnd, 0, (int)(view->logicScrollPos.y - logicYFromReal(view)) * view->symSize.y, &(view->clientRect), NULL);
        view->logicScrollPos.y = logicYFromReal(view);
        SendMessage(hwnd, WM_INVALIDRECT_BOTTOM, NULL, NULL);
    }
}

/**
 *
 * @brief function handles a vscroll action, updates scrolls, scrolls windows and calls InvalidRect if it was appear
 */
static void wm_hscroll_handler(HWND hwnd, WPARAM wParam, LPARAM lParam, view_model * view, store_model * store) {
    int scrollXInc = 0;
    switch(LOWORD(wParam)) {
        case SB_LINEUP:
            if (view->realScrollPos.x > 0)
                scrollXInc = LEFT;
            break;

        case SB_LINEDOWN:
            if (view->realScrollPos.x < view->realScrollRange.x)
                scrollXInc = RIGHT;
            break;

        case SB_PAGEUP :
            break;

        case SB_PAGEDOWN :
            break;

        case SB_THUMBPOSITION :
            scrollXInc = HIWORD(wParam) - view->realScrollPos.x;
            break;
        default : break;
    }

    if (scrollXInc) {
        view->realScrollPos.x += scrollXInc;

        SetScrollPos(hwnd, SB_HORZ, view->realScrollPos.x, TRUE);
        ScrollWindow(hwnd, (int)(view->logicScrollPos.x - logicXFromReal(view)) * view->symSize.x, 0, &(view->clientRect), NULL);
        view->logicScrollPos.x = logicXFromReal(view);
        SendMessage(hwnd, WM_INVALIDRECT_RIGHT, NULL, NULL);
    }
}

/**
 *
 * @brief It function was calls after general func in main.cpp and handles message in default mode
 */
LRESULT dmWindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam, store_model * store, view_model * view) {
    PAINTSTRUCT ps;
    HDC hdc;

    switch (message)                  /* handle the messages */
    {
        case WM_DESTROY:
            PRINT_MESSEGE(WM_DESTROY)
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;
        case WM_PAINT:
            PRINT_MESSEGE(WM_PAINT)
            hdc = BeginPaint(hwnd, &ps);
            drawTextRect(hdc, ps.rcPaint, view, store);
            EndPaint(hwnd, &ps);
            break;
        case WM_SIZE:
            PRINT_MESSEGE(WM_SIZE)
            wm_size_handler(hwnd, wParam, lParam, view, store);
            break;
        case WM_KEYDOWN:
            PRINT_MESSEGE(WM_KEYDOWN)
            wm_keyboard_handler(hwnd, wParam, lParam, view, store);
            break;
        case  WM_VSCROLL:
            PRINT_MESSEGE(WM_VSCROLL)
            wm_vscroll_handler(hwnd, wParam, lParam, view, store);
            break;
        case WM_HSCROLL:
            PRINT_MESSEGE(WM_HSCROLL)
            wm_hscroll_handler(hwnd, wParam, lParam, view, store);
            break;
        default:
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
};

/**
 *
 * @param logicScrollPosY - number of first line in the window
 * @brief sets default mode scrolls options and call InvalidateRect
 */
void startDefaultWithScrollYPos(HWND hwnd, store_model *store, view_model *view, long logicScrollPosY) {
    view->logicScrollPos.x = 0;
    view->logicScrollRange.x = max(store->capacity.x - view->winSizePixels.x / view->symSize.x, 0);
    view->logicScrollRange.y = max(store->capacity.y - view->winSizePixels.y / view->symSize.y, 0);
    view->logicScrollPos.y = min(logicScrollPosY, view->logicScrollRange.y);
    InvalidateRect(hwnd, &(view->clientRect), TRUE);
}