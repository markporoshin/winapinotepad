//
// Created by markp on 18.10.2019.
//

#ifndef WINAPI_DEFAULT_MODE_H
#define WINAPI_DEFAULT_MODE_H

#include "../../models.h"

LRESULT dmWindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam, store_model * store, view_model * view);
void startDefaultWithScrollYPos(HWND hwnd, store_model *store, view_model *view, long logicScrollPosY);
#endif //WINAPI_DEFAULT_MODE_H
