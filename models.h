//
// Created by markp on 18.10.2019.
//
#pragma once

#ifndef WINAPI_MODELS_H
#define WINAPI_MODELS_H

#include <windows.h>
#include "utils/vec.h"

#define realXFromLogic(vm) ((double)(vm)->logicScrollPos.x / (vm)->logicScrollRange.x * (vm)->realScrollRange.x)
#define realYFromLogic(vm) ((double)(vm)->logicScrollPos.y / (vm)->logicScrollRange.y * (vm)->realScrollRange.y)
#define logicXFromReal(vm) ((double)(vm)->realScrollPos.x / (vm)->realScrollRange.x * (vm)->logicScrollRange.x)
#define logicYFromReal(vm) ((double)(vm)->realScrollPos.y / (vm)->realScrollRange.y * (vm)->logicScrollRange.y)

typedef struct {
    OPENFILENAME ofn;
    char ** lines;
    vec2l capacity;
} store_model;

typedef struct tag_view_model view_model;
struct tag_view_model{
    vec2i winSizePixels;
    vec2i winSizeSyms;
    vec2i symSize;

    vec2l logicScrollRange;
    vec2l logicScrollPos;
    vec2i realScrollRange;
    vec2i realScrollPos;

    RECT clientRect;
    RECT invalidRectBelow;
    RECT invalidRectRight;

    void * auxiliary;
};



#endif //WINAPI_MODELS_H
