//
// Created by markp on 30.10.2019.
//

#ifndef WINAPI_WINAPI_MENU_H
#define WINAPI_WINAPI_MENU_H


#define IDM_OPEN     1
#define IDM_EXIT     2

#define IDM_TYPESET  3
#define IDM_DEFAULT  4

#define IDM_OK    32512


#endif //WINAPI_WINAPI_MENU_H
