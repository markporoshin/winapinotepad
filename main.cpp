

#include "config.h"

#include <windows.h>
#include <tchar.h>
#ifdef DEBUG
    #include <stdio.h>
#endif

#include "application.h"
#include "resources/winapi_menu.h"
#include "mode/default/default_mode.h"
#include "mode/typeset/typeset_mode.h"
#include "utils/pop_file.h"
#include "mode/general/general.h"


LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
TCHAR szClassName[ ] = _T("Lab");


int WINAPI WinMain (HINSTANCE hThisInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpszArgument,
                    int nCmdShow)
{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_OWNDC | CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = "WinApi";                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx(&wincl))
    return 0;



    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx(
            0,                   /* Extended possibilites for variation */
            szClassName,         /* Classname */
            _T("Win32"),       /* Title Text */
            WS_OVERLAPPEDWINDOW | WS_VSCROLL | WS_HSCROLL, /* default window */
            CW_USEDEFAULT,       /* Windows decides the position */
            CW_USEDEFAULT,       /* where the window ends up on the screen */
            550,                 /* The programs width */
            350,                 /* and height in pixels */
            HWND_DESKTOP,        /* The window is a child-window to desktop */
            NULL,                /* No menu */
            hThisInstance,       /* Program Instance handler */
            lpszArgument         /* No Window Creation data */
    );

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}

/**
 *
 * @param app       - structure with application state and date
 * @brief           - transfer message into current handler depending on mode
 */
LRESULT CALLBACK transferMsg2Handler(application * app, HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
    switch (app->mode) {
        case TYPESET:
            return tsWindowProcedure(hwnd, message, wParam, lParam, &(app->store), &(app->view));
        case DEFAULT:
            return dmWindowProcedure(hwnd, message, wParam, lParam, &(app->store), &(app->view));
    }
}


/**
 *
 * @brief handle message and call @see transferMsg2Handler
 * @return
 */
LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    static application app;

    HDC hdc;
    TEXTMETRIC   tm;
    static char fileName[MAX_LEN];

    switch (message) {
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
                case IDM_OPEN: {
                    free(app.store.lines[0]);
                    popFileOpenDlg(hwnd, fileName, &(app.store.ofn));
                    if(initStoreModel(fileName, &(app.store))) {
                        PostQuitMessage(1);
                    }
                    switch (app.mode) {
                        case TYPESET:
                            app.view.logicScrollPos.y = 0;
                            startTypesetFromView(hwnd, &(app.store), &(app.view));
                            break;
                        case DEFAULT:
                            startDefaultWithScrollYPos(hwnd, &(app.store), &(app.view), 0);
                    }
                    break;
                }
                case IDM_EXIT:
                    PostQuitMessage(0);
                    break;
                case IDM_TYPESET:
                    if (app.mode == DEFAULT)
                        startTypesetFromView(hwnd, &(app.store), &(app.view));

                    app.mode = TYPESET;
                    break;
                case IDM_DEFAULT:
                    if (app.mode == TYPESET)
                        startDefaultWithScrollYPos(hwnd, &(app.store), &(app.view), getCurrLineAndFinish(&(app.view)));

                    app.mode = DEFAULT;
                    break;
            }
            return 0;
        case WM_CREATE:
            app.mode = DEFAULT;
            popFileInitialize(hwnd, &(app.store.ofn));
            if(initStoreModel((char *) (LPSTR) ((CREATESTRUCT *) lParam)->lpCreateParams, &(app.store))) {
                PostQuitMessage(1);
            }
            hdc = GetDC(hwnd);
            SelectObject(hdc, GetStockObject(SYSTEM_FIXED_FONT));

            GetTextMetrics(hdc, &tm);
            app.view.symSize.x = tm.tmAveCharWidth;
            app.view.symSize.y = tm.tmHeight + tm.tmExternalLeading;
            app.view.logicScrollPos = {0, 0};
            app.view.realScrollPos = {0, 0};
            ReleaseDC(hwnd, hdc);
            return transferMsg2Handler(&app, hwnd, message, wParam, lParam);
        case WM_SIZE:
            app.view.winSizePixels.x = LOWORD(lParam);
            app.view.winSizePixels.y = HIWORD(lParam);
            app.view.winSizeSyms.x = app.view.winSizePixels.x / app.view.symSize.x;
            app.view.winSizeSyms.y = app.view.winSizePixels.y / app.view.symSize.y;

            app.view.clientRect.top = 0;
            app.view.clientRect.left = 0;
            app.view.clientRect.bottom = app.view.symSize.y * (app.view.winSizePixels.y / app.view.symSize.y);
            app.view.clientRect.right = app.view.symSize.x * (app.view.winSizePixels.x / app.view.symSize.x);
            app.view.invalidRectBelow = {
                    0,
                    app.view.clientRect.bottom,
                    app.view.winSizePixels.x,
                    app.view.winSizePixels.y
            };
            app.view.invalidRectRight = {
                    app.view.clientRect.right,
                    0,
                    app.view.winSizePixels.x,
                    app.view.winSizePixels.y
            };
            return transferMsg2Handler(&app, hwnd, message, wParam, lParam);
        case WM_QUIT:
            free(app.store.lines[0]);
            return transferMsg2Handler(&app, hwnd, message, wParam, lParam);
        case WM_INVALIDRECT_RIGHT:
            PRINT_MESSEGE(WM_INVALIDRECT_RIGHT);
            InvalidateRect(hwnd, &(app.view.invalidRectRight), TRUE);
            return 0;
        case WM_INVALIDRECT_BOTTOM:
            PRINT_MESSEGE(WM_INVALIDRECT_BOTTOM);
            InvalidateRect(hwnd, &(app.view.invalidRectBelow), TRUE);
            return 0;
        default:
            return transferMsg2Handler(&app, hwnd, message, wParam, lParam);

    }
}