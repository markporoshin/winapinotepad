//
// Created by markp on 31.10.2019.
//

#ifndef WINAPI_POP_FILE_H
#define WINAPI_POP_FILE_H

#include <windows.h>
#include <afxres.h>

BOOL popFileOpenDlg(HWND hwnd, PSTR pstrFileName, OPENFILENAME * ofn);
void popFileInitialize(HWND hwnd, OPENFILENAME * ofn);


#endif //WINAPI_POP_FILE_H
