//
// Created by markp on 18.10.2019.
//

#ifndef WINAPI_VEC_H
#define WINAPI_VEC_H

typedef struct {
    int x;
    int y;
} vec2i;

typedef struct {
    long x;
    long y;
} vec2l;

#endif //WINAPI_VEC_H
