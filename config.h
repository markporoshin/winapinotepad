//
// Created by markp on 30.10.2019.
//

#ifndef WINAPI_CONFIG_H
#define WINAPI_CONFIG_H

#define DEBUG


#define WM_INVALIDRECT_RIGHT (WM_USER + 0x0001)
#define WM_INVALIDRECT_BOTTOM (WM_USER + 0x0002)
#define MAX_LEN 256

#ifdef DEBUG
    #define PRINT_BORDER_SCROLL(view) \
    printf("border - (top: %ld,%d)\t(bottom: %ld,%d)\t(logicScroll: %ld)\t(realScroll: %d)\n", AUXILIARY(view)->top.line, AUXILIARY(view)->top.sym,\
    AUXILIARY(view)->bottom.line, AUXILIARY(view)->bottom.sym, (view)->logicScrollPos.y, (view)->realScrollPos.y);\
    fflush(stdout);

    #define PRINT_SIZE(view) \
    printf("size - (%ld)\n", view->logicScrollRange.y); \
    fflush(stdout);

    #define PRINTE_COUNT_OF_LINES(store) \
    printf("count of lines - (%ld)\n", store->capacity.y); \
    fflush(stdout);

    #define PRINT_MESSEGE(msg) \
    printf("message - " #msg "\n"); \
    fflush(stdout);
#else
    #define PRINT_BORDER_SCROLL(view)
    #define PRINT_SIZE(view)
    #define PRINTE_COUNT_OF_LINES(store)
    #define PRINT_MESSEGE(msg)
#endif


#endif //WINAPI_CONFIG_H
